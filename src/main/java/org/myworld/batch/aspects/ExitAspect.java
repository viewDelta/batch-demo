package org.myworld.batch.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class ExitAspect {

    @Autowired
    private ApplicationContext context;

    @After("execution(* org.myworld.batch.service.Process.run(..))")
    public void doExit(){
        log.warn("Shutting down batch application from ExitAspect");
        SpringApplication.exit(context);
    }

}
