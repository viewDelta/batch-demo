package org.myworld.batch.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class Process {

    @EventListener(ApplicationReadyEvent.class)
    public void run(){
        log.debug("Entering Process.run() method");
        for(int i=0; i<5; i++){
            log.info("Executing Task : {}", i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("Exception occured : {}", e);
            }
        }
        log.debug("Exiting Process.run() method");
    }

}
