FROM alpine:3.15.2
RUN apk update && apk upgrade && apk add openjdk8
COPY build/libs/*jar /usr/local/app.jar
WORKDIR "/usr/local/"
CMD ["java", "-jar", "app.jar"]