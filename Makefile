include .env
export

.PHONY: build
build:
	echo "Step:1 ===>>> Building Jar"
	./gradlew clean build
	echo "Step:2 ===>>> Building Docker Image and Tagging"
	docker build -t ${repo}:${version} .
	docker tag batch-demo:${version} ${ecr}/${repo}:${version}
	echo "Step:3 ===>>> Removing dangling images"
	docker image prune -f && docker images
	echo "Step:4 ===>>> Pushing image to ECR"
	aws ecr-public get-login-password --region ${region} | docker login --username AWS --password-stdin ${ecr}
	docker push ${ecr}/${repo}:${version}


