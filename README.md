# README #

This is a dummy batch application for illustration of aws batch with EventBridge.

### What is this repository for? ###

* Project requires java 8 to run
* Version - 1.0

### How do I get set up? ###

1. aws-cli has been already configured for the user (with `aws configure`) with accessId and accessKey [https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html]
2. Create Public AWS ECR and configure the require environment variables in **.env** file
3. Run the Makefile using command `make build` which will build the code, image and push the image to ECR
